# README #

The code in this repository is being released under the terms of the Gnu Public License v. 3.0 (GPL V. 3, http://www.gnu.org/copyleft/gpl.html), all rights are reserved however you may freely download, modify, alter, change, distribute, or otherwise do whatever you want to the code contained herein provided the following conditions are met:

1.  You make no claim of ownership to the base code as it is seen in this repository
2.  You Either branch or fork this code so the base code contained here doesn't get altered... even better, download the code and modify the copy.
3.  You abide by the conditions set forth in the GPL v. 3
4.  If you redistribute the code a copy of the terms of the GPL v.3 must be included with the distribution.
5.  You may NOT sell copies of this code or otherwise gain financially from it.

There are no warranties on this code, and no support is provided with it.

### What is this repository for? ###

The code contained in this repository is a very simple blog written in Ruby using Rails. I wrote it for a class project and now that the project is over I have decided to release it for educational use. I thought it might be a useful platform for anyone learning Ruby to have something to start out with. Feel free to grab a copy and play with it as much as you want. No attribution is required although if you do something really cool with it I'd love to hear about it and/or see it.

### How do I get set up? ###

First off you'll need a Ruby environment to code in. You'll also need to make sure the Rails gem is installed. After that it's a matter of copying over the code in this repository to your computer. You're on your own for this part, but make sure when you copy the files you maintain the EXACT directory structure in your project, otherwise things will break.